from pathlib import Path


BASE_DIR = Path(__file__).parents[1]
DATA_DIR = f'{BASE_DIR}/data'


def read_file_logs(file):
    with open(file, 'r', encoding='utf-8') as f:
        for line in f:
            yield line


def execute_query(cmd, value, data, file_name):
    path = f'{DATA_DIR}/{file_name}'
    if data is None:
        data_ = read_file_logs(path)
    else:
        data_ = data

    return list(CMD[cmd](value, data_))


def filter_query(value, data):
    return list(filter(lambda v: value in v, data))


def map_query(value, data):
    return list(map(lambda text: text.split(' ')[int(value)], data))


def unique_query(data):
    return list(set(data))


def sort_query(value, data):
    return sorted(data, reverse=True if value == 'desc' else False)


def limit_query(value, data):
    return data[:int(value)]


CMD = {
    'filter': filter_query,
    'map': map_query,
    'unique': unique_query,
    'sort': sort_query,
    'limit': limit_query,
}
